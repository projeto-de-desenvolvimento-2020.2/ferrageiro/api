# API

### Repositório da API do projeto

## Tecnologias
[Python](https://www.python.org/)

[Framework Flask](https://flask.palletsprojects.com/en/1.1.x/) 

[XAMPP](https://www.apachefriends.org/pt_br/index.html) - MYSQL - APACHE
## Requisitos para instalar o projeto
Basta instalar o Python e logo executar os comandos via terminal.

* **Python** - https://www.python.org/
* **Flask** - pip install Flask
* **Flask-Blueprint** - pip install flask-Blueprint
* **Flask-SQLAlchemy** - pip install flask-SQLAlchemy
* **Flask-CORS** - pip install flask-cors
* **JWT-Extended** - pip install flask-jwt-extended
* **PyMYSQL** - pip install pymsql

## Clone do projeto

Basta clicar [aqui](https://gitlab.com/projeto-de-desenvolvimento-2020.2/ferrageiro/api.git) e logo clicar em clone.

## Execução da API

1. Inicie o MySQL e APACHE (XAMPP);
2. Execute o app.py na pasta da API;
3. Abra o navegador com o link da API (http://localhost:5000)
4. Para criar o banco de dados, basta adicionar a rota criadb no final da URL (http://localhost:5000/criadb), então o banco de dados será criado, logo poderá ser feito inserção, consulta, exclusão de dados na API.

**Dica**: Para testar a API, utilize Insomnia ou Postman (ou outro de preferência).